import React from 'react';
import { connect } from 'react-redux';
import { composeErrorHandlers, createUnexpectedErrorHandler, submissionErrorHandler } from '../../utils/errorUtils';
import { acUpdateClient } from '../../actions/clientProfileActions';
import createCustomizableForm from './сreateCustomizableForm';
import { getClientProfileFormFields } from './clientProfileSelectors';

const mapProfileToFormValues = ( profile ) => ({
  firstName: profile.firstName,
  lastName: profile.lastName,
  username: profile.username,
  email: profile.email,
  departmentId: profile.departmentId
});

const mapStateToProps = ( state ) => ({
  initialValues: mapProfileToFormValues( state.clientProfile.profile ),
  fields: getClientProfileFormFields( state ),
  submitButtonText: 'Submit',
  cancelButtonText: 'Clear',
  showSubmitButton: true,
  showCancelButton: true,
  successMessage: 'Client profile was successfully updated!'
});

const mapDispatchToProps = ( dispatch ) => ({
  onSubmit: ( profile ) =>
    dispatch( acUpdateClient( profile ) )
      // Displays errors
      .catch( composeErrorHandlers(
        submissionErrorHandler,
        createUnexpectedErrorHandler( dispatch )
      ) )
});

export default connect( mapStateToProps, mapDispatchToProps )( createCustomizableForm( { form: 'editClientProfile' } ) );