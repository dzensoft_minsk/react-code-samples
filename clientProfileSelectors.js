import { createSelector } from 'reselect';
import { Field } from 'redux-form';
import { Input, Select } from './CustomizableForm';
import { mapDepartmentsToFormOptions } from './studentsCreatorFormSelectors';

const getDepartments = ( state ) => state.clientProfile.departments;

export const getClientProfileFormFields = createSelector(
  getDepartments,
  ( departments ) => [
    {
      component: Field,
      componentProps: {
        name: 'username',
        type: 'text',
        label: 'Login:',
        component: Input,
        placeholder: 'Login'
      }
    },
    {
      component: Field,
      componentProps: {
        name: 'firstName',
        type: 'text',
        label: 'First Name:',
        component: Input,
        placeholder: 'First name',
      }
    },
    {
      component: Field,
      componentProps: {
        name: 'lastName',
        type: 'text',
        label: 'Last Name:',
        component: Input,
        placeholder: 'Last name'
      }
    },
    {
      component: Field,
      componentProps: {
        name: 'email',
        type: 'text',
        label: 'Email:',
        component: Input,
        placeholder: 'Email'
      }
    },
    {
      component: Field,
      componentProps: {
        name: 'departmentId',
        label: 'Department:',
        component: Select,
        options: mapDepartmentsToFormOptions( departments )
      }
    }
  ]
);