import reduxForm from 'redux-form/es/reduxForm';
import CustomizableForm from './CustomizableForm';

export default ( options ) => reduxForm( options )( CustomizableForm );