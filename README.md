##Code Description

- `CustomizableForm` is a component that displays form fields with error messages or success messages. By default for submitting the form `Buttons` component is used, but it can be override.
- `Buttons` is a component that contains a submit button and a cancel button. Depending on the props submit button can be disabled or show the loading process (loading indicator).
- `CreateCustomizableForm` is a function that helps to create `CustomizableForm` with options.
- `EditClientProfile` is a component that displays client profile fields. This component uses a `CustomisableForm` to display and edit form fields. The initial values ​​for the form are taken from the `Redux` state.If any errors appear in the form field, they will be displayed below the field containing the error.
- `ClientProfileSelector` is a file with a function that returns an array of form fields. Each form field is a javascript object that contains component field `Field` and component props which can contain any necessary property. `Field` is a component from the `Redux-Form` library, but any other component can be used instead of it. The client profile requires a list of departments,therefore, the function `selector` which caches form fields for the same departments is used.
