import React from 'react';
import { compose, pure } from 'recompose';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { map } from 'lodash-es';
import Buttons from './Buttons';
import BaseInput from './controls/BaseInput';
import BaseSelect from './controls/BaseSelect';
import { withLabel } from './withLabel';
import { withError } from './withError';

// Input field that can display the label and error
export const Input = compose(
  withLabel,
  withError,
  pure
)( BaseInput );

// Select field that can display the label and error
export const Select = compose(
  withLabel,
  withError,
  pure
)( BaseSelect );

class CustomizableForm extends React.PureComponent {
  componentDidMount() {
    this.props.onLoad && this.props.onLoad();
  }

  handleCancelButtonClick = ( event ) => {
    event.preventDefault();
    this.props.reset && this.props.reset();
    this.props.onCancelButtonClick && this.props.onCancelButtonClick();
  };

  render() {
    const {
      fields, submitButtonText, cancelButtonText, showSubmitButton, showCancelButton, handleSubmit, successMessage,
      formButtons: FormButtons, submitSucceeded, submitting, error, isLoading, className
    } = this.props;

    return (
      <div className={ classNames( 'custom-form', className ) }>
        /* Shows success message */
        { submitSucceeded && !submitting && (
          <div className='submit-succeed'>
            { successMessage }
          </div>
        ) }
        /* Shows an error message for the entire form */
        { error && (
          <div className='submit-failed'>
            { error }
          </div>
        ) }
        <form onSubmit={ handleSubmit }>
          {
            map( fields, ( fieldProps, i ) =>
              (typeof fieldProps.show === 'undefined' || fieldProps.show()) ?
                React.createElement( fieldProps.component, { ...fieldProps.componentProps, key: i } ) : null
            )
          }
          <FormButtons
            onSubmit={ this.props.onSubmit }
            handleSubmit={ handleSubmit }
            handleCancel={ this.handleCancelButtonClick }
            submitButtonText={ submitButtonText }
            showSubmitButton={ showSubmitButton }
            cancelButtonText={ cancelButtonText }
            showCancelButton={ showCancelButton }
            isLoading={ isLoading }
          />
        </form>
      </div>
    )
  }
}

const fieldType = {
  component: PropTypes.func.isRequired,
  show: PropTypes.func,
  componentProps: PropTypes.shape( {
    name: PropTypes.string,
    type: PropTypes.string,
    label: PropTypes.string,
    component: PropTypes.func,
    placeholder: PropTypes.string
  } )
};

CustomizableForm.propTypes = {
  fields: PropTypes.arrayOf(
    PropTypes.shape( fieldType ) ),
  ...Buttons.propTypes,
  onLoad: PropTypes.func,
  handleSubmit: PropTypes.func,
  onCancelButtonClick: PropTypes.func,
  successMessage: PropTypes.string,
  submitSucceeded: PropTypes.bool,
  submitting: PropTypes.bool,
  error: PropTypes.string,
  className: PropTypes.string,
  isLoading: PropTypes.bool,
  buttonsComponent: PropTypes.func
};

CustomizableForm.defaultProps = {
  formButtons: Buttons
};

export default CustomizableForm;
