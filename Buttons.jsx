import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';

class Buttons extends React.PureComponent {
  render() {
    const { handleSubmit, handleCancel, submitButtonText, cancelButtonText, showSubmitButton, showCancelButton, isLoading, isDisabled } = this.props;

    return (
      <div className='buttons'>
        {
          showSubmitButton &&
          <Button
            type='submit'
            onClick={ handleSubmit }
            loading={ isLoading }
            disabled={ isDisabled || isLoading }
          >
            { submitButtonText }
          </Button>
        }
        {
          showCancelButton &&
          <button className='button-invert' onClick={ handleCancel }>{ cancelButtonText }</button>
        }
      </div>
    )
  }
}

Buttons.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string,
  cancelButtonText: PropTypes.string,
  showSubmitButton: PropTypes.bool,
  showCancelButton: PropTypes.bool,
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  handleCancel: PropTypes.func
};

export default Buttons;